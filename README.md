# equipment-rent

[![License:MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


# 2019.1
 
It's a collaborative project to have a purpose to demonstrate our skills in management project, and development of software, we hope that you enjoy our journey.

## Sobre o Projeto

<p align="justify"> Este software tem como intuito auxiliar no controle de emprestimo de equipamentos de TI. Afim de se obter um maior controle, sobre estoque de equipamentos e proporcionando uma melhor gestão sobre alguns ativos de ti.</p>

## Instalação

adicionar guia de instalação e configuração da aplicação.


Faça clone ou download do repositório.

```console
$ git clone https://gitlab.com/jonathan.cabral/equipment-rent
```

## Principais Features

O projeto Rent tem como principais features:

* Cadastro de Gestores 
* Cadastro de Usuarios
* Cadastro de área
* Gerenciamento dos clientes 
* Cadastro de equipamentos
* Emprestimos de equipamento
* Gerenciamento de equipamentos
* Geração de relatórios
* Envio de notificação via email

## Documentação do Projeto

Visualize a documentação na nossa [wiki](https://gitlab.com/jonathan.cabral/equipment-rent/wikis/home).


 
