package com.equipments.rent.repository;

import com.equipments.rent.model.Prefix;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrefixRepository extends JpaRepository<Prefix, Long> {
}
