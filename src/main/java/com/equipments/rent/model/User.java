package com.equipments.rent.model;

import com.equipments.rent.util.enums.TypeUserEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name =  "TB_USER")
public @Data class User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @NotNull
    @Size(max = 45)
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotBlank
    @NotNull
    @Size(max = 45)
    @Column(name = "LAST_NAME")
    private String lastName;

    @NotNull
    @Column(name = "REGISTRATION")
    private Long registration;

    @NotBlank
    @NotNull
    @Size(max = 45)
    @Column(name = "LOGIN")
    private String login;

    @NotBlank
    @NotNull
    @Size(max = 45)
    @Column(name = "PASSWORD")
    private String password;

    @NotNull
    @Column(name = "TYPE_USER")
    private TypeUserEnum typeUser;

    @NotNull
    @ManyToOne()
    @JoinColumn(name = "TB_AREA_ID", referencedColumnName = "id")
    private Area area;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Loan> loans;
}
