package com.equipments.rent.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "TB_PREFIX")
public @Data class Prefix extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    @Size(max = 6)
    @Column(name =  "PREFIX")
    private String prefix;

    @NotNull
    @NotBlank
    @Size(max = 45)
    @Column(name = "DESCRIPTION")
    private String description;
}
