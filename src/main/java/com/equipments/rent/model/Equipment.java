package com.equipments.rent.model;

import com.equipments.rent.util.enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "TB_EQUIPMENT")
public @Data class Equipment extends  BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @NotNull
    @Size(max = 45)
    @Column(name = "NAME")
    private String name;

    @NotNull
    @ManyToOne()
    @JoinColumn(name = "PREFIX", referencedColumnName = "id")
    private Prefix prefix;

    @NotNull
    @Column(name = "DT_INCLUSION")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date dtInclusion;

    @NotNull
    @Column(name = "STATUS")
    private StatusEnum status;
}
