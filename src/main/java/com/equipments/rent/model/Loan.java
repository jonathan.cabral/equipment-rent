package com.equipments.rent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "TB_LOAN")
public class Loan extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "TB_USER")
    private User user;

    @NotNull
    @Column(name = "TB_EQUIPMENT")
    @JoinColumn(name = "ID")
    private Equipment equipment;

    @NotNull
    @Column(name = "TB_ADM_USER")
    @JoinColumn(name = "ID")
    private User userAdministrator;

    @NotNull
    @Column(name = "DT_START")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date dtStart;

    @Column(name = "DT_END")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date dtEnd;

    @Column(name = "DT_DELIVERED")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date dtDelivered;
}
