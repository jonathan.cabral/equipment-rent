package com.equipments.rent.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum StatusEnum {

    ACTIVE(1), INACTIVE(2), IN_USE(3), MANUTENTION(4), BROKEN(5);

    private int statusCode;
}
