package com.equipments.rent.util.enums;

public enum TypeLoanEnum {

	FIX(1), RENT(2);
	
	private Integer code;

	private TypeLoanEnum(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return this.code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
}
