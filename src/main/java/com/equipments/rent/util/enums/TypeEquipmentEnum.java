package com.equipments.rent.util.enums;

public enum TypeEquipmentEnum {

	NOTEBOOK(1, "NOTEBOOK", "NBK"), 
	COMPUTADOR(2, "COMPUTADOR", "DSK"), 
	ENERGIA(3, "ENERGIA", "ENG"),
	PREFIFERICOS(4, "PERIFERICO", "PER");

	private Integer tipo;
	private String desc;
	private String prefix;

	private TypeEquipmentEnum(Integer tipo, String desc, String prefix) {
		this.desc = desc;
		this.tipo = tipo;
		this.prefix = prefix;
	}

	public TypeEquipmentEnum getEquipamentoByType(Integer type) {
		if (type == null)
			return null;

		for (TypeEquipmentEnum t : TypeEquipmentEnum.values()) {
			if (t.getTipo().equals(type))
				return t;
		}

		return null;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
