package com.equipments.rent.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum TypeUserEnum {

    ADMINISTRATOR(1), COMMON(2), VISITOR(3);

    private int code;

}
