package com.equipments.rent.controllers;

import com.equipments.rent.model.User;
import com.equipments.rent.services.UserService;
import io.swagger.annotations.Api;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@RestController
@RequestMapping("/users")
@Api(value = "Users", protocols = "HTTPS", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
public class UserResource {

    @Autowired UserService service;

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<User>  save(@Valid @RequestBody User user) {
        try {
			return  ResponseEntity.status(HttpStatus.CREATED).body(service.save(user));
		} catch (Exception e) {
			throw new HibernateException("It's not possible to save this user \n" + e.getMessage());
		}
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON, value = "{id}")
    public ResponseEntity<User> findAll(@PathVariable("id") Long id) {
       User user = service.find(id).orElseThrow(() -> new NoResultException("No user was found for this id " + id));
       return ResponseEntity.status(HttpStatus.FOUND).body(user);
    }


}
