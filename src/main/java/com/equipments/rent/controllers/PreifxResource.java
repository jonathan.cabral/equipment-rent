package com.equipments.rent.controllers;

import com.equipments.rent.model.Prefix;
import com.equipments.rent.model.User;
import com.equipments.rent.services.PrefixService;
import com.equipments.rent.services.UserService;
import io.swagger.annotations.Api;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/prefixes")
@Api(value = "Prefixes", protocols = "HTTPS", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
public class PreifxResource {

    @Autowired PrefixService service;

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Prefix> save(@Valid @RequestBody Prefix prefix) {
        try {
            return  ResponseEntity.status(HttpStatus.CREATED).body(service.save(prefix));
        } catch (Exception e) {
            throw new HibernateException("It's not possible to save this user \n" + e.getMessage());
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON, value = "{id}")
    public ResponseEntity<Prefix> findAll(@PathVariable("id") Long id) {
        Prefix prefix = service.find(id).orElseThrow(() -> new NoResultException("No prefix was found for this id " + id));
        return ResponseEntity.status(HttpStatus.FOUND).body(prefix);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Prefix>  delete(@PathVariable(name = "id", required = true) Long id) {
        try {
            service.delete(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            throw new HibernateException("It's wasn't possible to delete this prefix", e);
        }
    }
}
