package com.equipments.rent.controllers;

import com.equipments.rent.model.Equipment;
import com.equipments.rent.model.Prefix;
import com.equipments.rent.services.EquipmentService;
import io.swagger.annotations.Api;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/equipments")
@Api(value = "Equipments", protocols = "HTTPS", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
public class EquipmentResource {

    @Autowired private EquipmentService service;

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Equipment> save(@Valid @RequestBody Equipment equipment) {
        try {
            return  ResponseEntity.status(HttpStatus.CREATED).body(service.save(equipment));
        } catch (Exception e) {
            throw new HibernateException("It's not possible to save this user \n" + e.getMessage());
        }
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON, value = "{id}")
    public ResponseEntity<Equipment> findAll(@PathVariable("id") Long id) {
        Equipment equipment = service.find(id).orElseThrow(() -> new NoResultException("No prefix was found for this id " + id));
        return ResponseEntity.status(HttpStatus.FOUND).body(equipment);
    }

}
