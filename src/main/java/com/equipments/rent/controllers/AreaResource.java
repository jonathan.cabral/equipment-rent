package com.equipments.rent.controllers;

import com.equipments.rent.model.Area;
import com.equipments.rent.services.AreaService;
import io.swagger.annotations.Api;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.NoResultException;
import javax.print.attribute.standard.Media;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/areas")
@Api(value = "Areas Resource", protocols = "HTTPS", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
public class AreaResource {

    @Autowired private AreaService service;

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Area> save(@RequestBody Area area) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(area));
        } catch (Exception e) {
            throw new HibernateException("It's not possible to save this area" + e.getMessage());
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<Area> find(@PathVariable(name = "id", required = true) Long id) {
        try {
            return ResponseEntity.status(HttpStatus.FOUND).body(service.find(id).orElseThrow(() -> new NoResultException("No area was found for this ID")));
        } catch (NoResultException e) {
            throw new NoResultException("No administrator was found for this id: " + id);
        }
    }


}
