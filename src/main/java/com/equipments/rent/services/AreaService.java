package com.equipments.rent.services;

import com.equipments.rent.model.Area;
import com.equipments.rent.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AreaService {

    @Autowired private AreaRepository repository;

    public Area save(Area area) {
        return repository.save(area);
    }

    public Optional<Area> find(Long id) {
        return repository.findById(id);
    }

    public List findAll() {
        return repository.findAll();
    }

}
