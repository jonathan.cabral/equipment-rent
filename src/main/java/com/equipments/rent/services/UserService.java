package com.equipments.rent.services;

import com.equipments.rent.model.Area;
import com.equipments.rent.model.User;
import com.equipments.rent.repository.UserRepository;
import com.equipments.rent.util.enums.TypeUserEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired private AreaService areaService;
    @Autowired UserRepository repository;

    public User save(User user) {
        return repository.save(user);
    }

    public Optional<User> find(Long id) {
        return repository.findById(id);
    }

    public List<User> findAll() {
        return repository.findAll();
    }

}
