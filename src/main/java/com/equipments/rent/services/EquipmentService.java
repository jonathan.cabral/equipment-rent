package com.equipments.rent.services;

import com.equipments.rent.model.Equipment;
import com.equipments.rent.repository.EquipmentRepository;
import com.sun.xml.internal.ws.developer.Serialization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EquipmentService {

    @Autowired private EquipmentRepository repository;

    public Equipment save(Equipment equipment) {
        return repository.save(equipment);
    }

    public Optional<Equipment> find(Long id) {
        return repository.findById(id);
    }

    public List<Equipment> find() {
        return repository.findAll();
    }

}
