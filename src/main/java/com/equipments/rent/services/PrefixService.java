package com.equipments.rent.services;

import com.equipments.rent.model.Prefix;
import com.equipments.rent.repository.PrefixRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrefixService {

    @Autowired private PrefixRepository repository;

    public Prefix save(Prefix prefix) {
        return repository.save(prefix);
    }

    public Optional<Prefix> find(Long id) {
        return repository.findById(id);
    }

    public List<Prefix> find() {
        return repository.findAll();
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }
 }
